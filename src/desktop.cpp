/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#include <iostream>
#include <fstream>
#include <filesystem>
namespace fs = std::filesystem;
#include <vector>
#include <set>
#include <map>

#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;
#include <boost/algorithm/string/replace.hpp>

#include "desktop.h"
#include "coba_utils.h"

void desktop::set_exec(const std::string &data)
{
	auto cleaned = std::string(data);
	boost::replace_all(cleaned, "%F", "");
	boost::replace_all(cleaned, "%U", "");
	boost::replace_all(cleaned, "%u", "");
	boost::replace_all(cleaned, "%L", "");
	this->exec = xml_escape(cleaned);
}

void desktop::set_name(const std::string &data)
{
	this->name = xml_escape(data);
}

std::string desktop::get_name() const
{
	return this->name;
}

void desktop::set_icon(const std::string &data)
{
	this->icon = xml_escape(data);
}

std::string desktop::get_icon() const
{
	return this->icon;
}

void desktop::set_terminal(const std::string &data)
{
	if (data == "True" || data == "true")
		this->terminal = true;
}

void desktop::set_no_display(const std::string &data)
{
	if (data == "True" || data == "true")
		this->no_display = true;
}

bool desktop::get_no_display() const
{
	return this->no_display;
}

void desktop::add_category(const std::string &data)
{
	this->categories.insert(xml_escape(data));
}

std::set<std::string> desktop::get_categories() const
{
	return this->categories;
}

void desktop::populate_aliases()
{
	desktop::aliases["Audio"] = "Multimedia";
	desktop::aliases["Video"] = "Multimedia";
	desktop::aliases["AudioVideo"] = "Multimedia";
	desktop::aliases["Network"] = "Internet";
	desktop::aliases["Game"] = "Games";
	desktop::aliases["Utility"] = "Utilities";
	desktop::aliases["Development"] = "Editors";
	desktop::aliases["GTK"] = "";
	desktop::aliases["GNOME"] = "";
}

desktop::desktop(fs::path path, const std::string &terminal)
{
	if (desktop::aliases.empty())
		populate_aliases();
	if (desktop::locale_lang.empty())
		desktop::locale_lang =
			"Name[" +
			std::use_facet<boost::locale::info>(std::locale())
				.language() +
			"]";
	if (desktop::locale_country.empty())
		desktop::locale_country =
			"Name[" +
			std::use_facet<boost::locale::info>(std::locale())
				.language() +
			"_" +
			std::use_facet<boost::locale::info>(std::locale())
				.country() +
			"]";
	std::ifstream ifs(path.string());
	bool active = false;
	bool named = false;
	bool named_exact = false;
	bool named_lang = false;
	bool dnd = false;

	std::string line;
	while (ifs) {
		std::getline(ifs, line);
		if (line == initializer) {
			active = true;
			continue;
		}
		if (!active) {
			continue;
		}
		if (line.length() < 6 || line.at(0) == '#') {
			continue;
		}
		int brace_pos = line.find('[');
		/* only one tag will be processed, a single [Desktop Entry] */
		if (brace_pos == 0 && !(line == initializer)) {
			return;
		}
		auto pos = line.find('=');
		if (pos == std::string::npos) {
			continue;
		}

		std::string key = line.substr(0, pos);
		std::string value = line.substr(pos + 1);

		if ((!(named_exact || named_lang)) && key == name_lookup) {
			this->set_name(value);
			named = true;
			if (dnd)
				return;
			continue;
		}

		if ((!named_exact) && key == locale_lang) {
			this->set_name(value);
			named = true;
			named_lang = true;
			if (dnd)
				return;
			continue;
		}

		if (key == locale_country) {
			this->set_name(value);
			named = true;
			named_lang = true;
			named_exact = true;
			if (dnd)
				return;
			continue;
		}

		if (key == exec_lookup) {
			this->set_exec(value);
			if (this->terminal) {
				this->set_exec(terminal + " " + this->exec);
			}
			continue;
		}
		if (key == icon_lookup) {
			this->set_icon(value);
			continue;
		}
		if (key == terminal_lookup) {
			this->set_terminal(value);
			if (this->terminal && this->exec != "") {
				this->set_exec(terminal + " " + this->exec);
			}
			continue;
		}
		if (key == no_display_lookup) {
			this->set_no_display(value);
			if (!named)
				dnd = true;
			else
				return;
		}
		if (key == categories_lookup) {
			if (value.empty())
				this->add_category("Other");
			std::vector<std::string> values =
				split_delim(value, ';');
			for (std::string cat : values) {
				auto it = aliases.find(cat);
				if (it != aliases.end()) {
					cat = it->second;
				}
				this->add_category(cat);
			}
			continue;
		}
	}
}

bool desktop::render(pt::ptree &tree)
{
	pt::ptree &inner_tree = tree.add_child("item", pt::ptree{});

	inner_tree.put("<xmlattr>.label", this->name);
	if (!this->icon.empty())
		inner_tree.put("<xmlattr>.icon", this->icon);
	inner_tree.put("action.<xmlattr>.name", "Execute");
	inner_tree.put("action.command", this->exec);

	// tree.add_child("item", inner_tree);

	return true;
}
