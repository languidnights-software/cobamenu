/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#ifndef COBA_DESKTOP_H
#define COBA_DESKTOP_H

#include <string>
#include <filesystem>
namespace fs = std::filesystem;
#include <vector>
#include <set>
#include <map>

#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;
#include <boost/locale.hpp>

class desktop {
    public:
	std::string filename;

	desktop(fs::path path, const std::string &terminal);

	void set_exec(const std::string &data);
	void set_icon(const std::string &data);
	std::string get_icon() const;
	void set_name(const std::string &data);
	std::string get_name() const;
	void set_terminal(const std::string &data);
	void set_no_display(const std::string &data);
	bool get_no_display() const;
	void add_category(const std::string &data);
	std::set<std::string> get_categories() const;

	bool render(pt::ptree &tree);

    private:
	std::string exec = "";
	std::string icon;
	std::string name;
	std::string comment;
	std::string type;
	bool terminal = false;
	bool no_display = false;
	std::set<std::string> categories;

	inline const static std::string initializer = "[Desktop Entry]";
	inline const static std::string name_lookup = "Name";
	inline const static std::string exec_lookup = "Exec";
	inline const static std::string icon_lookup = "Icon";
	inline const static std::string terminal_lookup = "Terminal";
	inline const static std::string no_display_lookup = "NoDisplay";
	inline const static std::string categories_lookup = "Categories";

	inline static std::map<std::string, std::string> aliases;
	inline static std::string locale_lang;
	inline static std::string locale_country;

	void populate_aliases();
};

#endif
