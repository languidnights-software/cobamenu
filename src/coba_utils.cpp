/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#include "coba_utils.h"

#include <iostream>
#include <fstream>
#include <filesystem>
namespace fs = std::filesystem;
#include <string>
#include <sstream>
#include <vector>
#include <set>
#include <unordered_set>
#include <map>

#include <boost/property_tree/ptree.hpp>
namespace pt = boost::property_tree;
#include <boost/algorithm/algorithm.hpp>
#include <boost/algorithm/string/replace.hpp>

std::string get_config_file()
{
	char *xdg_config_home_env = getenv("XDG_CONFIG_HOME");
	std::string config_home = "";
	if (xdg_config_home_env != NULL) {
		config_home = std::string(xdg_config_home_env);
	}
	char *homedir_env = getenv("HOME");
	std::string homedir = "";
	if (homedir_env != NULL) {
		homedir = std::string(homedir_env);
	}
	std::string config_file = ".cobamenu.rc";
	if (!config_home.empty()) {
		fs::path path(config_home + "/cobamenu");
		if (fs::exists(path)) {
			config_file = config_home + "/cobamenu/config";
		}
	} else {
		fs::path path(homedir + "/.config/cobamenu");
		if (fs::exists(path)) {
			config_file = homedir + "/.config/cobamenu/config";
		}
	}
	return config_file;
}

std::string get_icon_theme()
{
	fs::path path(get_config_file());
	if (fs::exists(path)) {
		std::ifstream config(path);
		std::string search("icon-theme-name");
		while (config) {
			std::string kvp;
			std::getline(config, kvp);
			if (kvp.find(search) != std::string::npos)
				return kvp.substr(search.length() + 1);
		}
	}
	return "hicolor";
}

std::string get_terminal_emulator()
{
	fs::path path(get_config_file());
	if (fs::exists(path)) {
		std::ifstream config(path);
		std::string search("terminal");
		while (config) {
			std::string kvp;
			std::getline(config, kvp);
			if (kvp.find(search) != std::string::npos)
				return kvp.substr(search.length() + 1);
		}
	}
	return "xterm -e";
}

std::vector<std::string> split_delim(std::string str, char delim)
{
	std::vector<std::string> output;
	std::string elem;

	std::stringstream strs(str);
	while (std::getline(strs, elem, delim)) {
		output.push_back(elem);
	}

	return output;
}

std::string xml_escape(std::string input)
{
	auto output = std::string(input);
	/* emacs has a weird .desktop file */
	boost::replace_all(output, "\\\\\"", "\\\"");
	boost::replace_all(output, "\\\\$", "\\$");

	boost::replace_all(output, "&", "&amp;");
	boost::replace_all(output, "\"", "&quot;");
	boost::replace_all(output, "'", "&apos;");
	boost::replace_all(output, "<", "&lt;");
	boost::replace_all(output, ">", "&gt;");
	return output;
}

std::vector<std::string> get_application_dirs()
{
	std::set<std::string> app_dirs;
	std::unordered_set<std::string> exclude_app_dirs;
	std::vector<std::string> final_app_dirs;
	fs::path path(get_config_file());
	bool search_all_dirs = false;

	if (fs::exists(path)) {
		std::ifstream gtkrc(path);
		std::string search("application-dir");
		std::string merge("merge-default-application-dirs");
		std::string excludes("exclude-application-dir");
		while (gtkrc) {
			std::string kvp;
			std::getline(gtkrc, kvp);
			if (kvp.find(search) != std::string::npos) {
				app_dirs.insert(
					kvp.substr(search.length() + 1));
			}
			if (kvp.find(merge) != std::string::npos) {
				std::string search_all_dirs_v =
					kvp.substr(merge.length() + 1);
				if (tolower(search_all_dirs_v.front()) == 't') {
					search_all_dirs = true;
				}
			}
			if (kvp.find(excludes) != std::string::npos) {
				exclude_app_dirs.insert(
					kvp.substr(excludes.length() + 1));
			}
		}
	}

	if (app_dirs.empty() || search_all_dirs) {
		std::string homedir(getenv("HOME"));
		app_dirs.insert(homedir + "/.local/share/applications");
		app_dirs.insert(
			homedir +
			"/.local/share/flatpak/exports/share/applications");
		app_dirs.insert("/var/lib/flatpak/exports/share/applications");
		app_dirs.insert("/usr/local/share/applications");
		app_dirs.insert("/usr/share/applications");
	}

	for (auto app_dir : app_dirs) {
		if (exclude_app_dirs.count(app_dir) == 0) {
			final_app_dirs.push_back(app_dir);
		}
	}

	return final_app_dirs;
}

std::vector<fs::path> collect_desktops()
{
	std::vector<fs::path> desktops;

	std::vector<std::string> dirs = get_application_dirs();

	std::unordered_set<std::string> seen;
	for (std::string dir : dirs) {
		fs::path path(dir);
		if (fs::exists(path) && fs::is_directory(fs::status(path))) {
			for (auto entry : fs::directory_iterator(path)) {
				if (seen.count(entry.path().stem()) == 0 &&
				    entry.path().extension() == ".desktop") {
					desktops.push_back(entry.path());
					seen.insert(entry.path().stem());
				}
			}
		}
	}

	return desktops;
}

std::string clean_xml(pt::ptree tree)
{
	std::ostringstream oss;
	pt::xml_parser::write_xml(oss, tree);

	auto cleaned_xml = oss.str();
	boost::replace_all(cleaned_xml, "&gt;", ">");
	boost::replace_all(cleaned_xml, "&lt;", "<");
	boost::replace_all(cleaned_xml, "&quot;", "\"");
	boost::replace_all(cleaned_xml, "&apos;", "'");
	boost::replace_all(cleaned_xml, "&amp;", "&");

	return cleaned_xml;
}
